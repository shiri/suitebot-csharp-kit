﻿using System;
using suitebot_Csharp_kit.BotAI;
using suitebot_Csharp_kit.Server;

namespace suitebot_Csharp_kit
{
	public class BotServer
	{
		private const int DefaultPort = 9001;

		public static int Main(string[] args)
		{
			IBotAi botAi = new SampleBotAi(); // replace with your own AI

			var port = DeterminePort(args);

			Console.Out.WriteLine("listening on port " + port);
			new SimpleServer(port, new BotRequestHandler(botAi)).Run();

			return 0;
		}

		private static int DeterminePort(string[] args)
		{
		  return args.Length == 1 ? int.Parse(args[0]) : DefaultPort;
		}
	}
}