﻿namespace suitebot_Csharp_kit.Server
{
	public interface ISimpleRequestHandler
	{
		string ProcessRequest(string request);
	}
}