﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;


namespace suitebot_Csharp_kit.Game
{
	public class ImmutableGameState : IGameState
	{
		private readonly List<int> _liveBotIds;
		private readonly Dictionary<int, Point> _botLocationMap;
		private readonly Dictionary<int, int> _botEnergyMap;
        private readonly int _width;
        private readonly int _height;
        private readonly List<int> _botIds;
        private readonly HashSet<Point> _obstacles;
        private readonly HashSet<Point> _treasures;
        private readonly HashSet<Point> _batteries;

        public int GetPlanWidth()
		{
			return _width;
		}

		public int GetPlanHeight()
		{
			return _height;
		}

		public List<int> GetAllBotIds()
		{
			return _botIds;
		}

		public List<int> GetLiveBotIds()
		{
			return _liveBotIds;
		}

		public Point GetBotLocation(int botId)
		{
			var location = _botLocationMap[botId];
			if (location == null)
				AssertKnownBotId(botId);
			return location;
		}

		public int GetBotEnergy(int botId)
		{
			return _botEnergyMap[botId];

		}

		public HashSet<Point> GetObstacleLocations()
		{
			return _obstacles;
		}

		public HashSet<Point> GetTreasureLocations()
		{
			return _treasures;
		}

		public HashSet<Point> GetBatteryLocations()
		{
			return _batteries;
		}

        public ImmutableGameState(int width, int height, List<int> botIds, Dictionary<int, int> botEnergyMap, Dictionary<int, Point> botLocationMap, HashSet<Point> obstacles, HashSet<Point> treasures, HashSet<Point> batteries)
        {
            this._width = width;
            this._height = height;
            _botIds = botIds;
            this._botEnergyMap = botEnergyMap;
            this._botLocationMap = botLocationMap;
            _liveBotIds = botIds.Where(botLocationMap.ContainsKey).ToList();
            this._obstacles = obstacles;
            this._treasures = treasures;
            this._batteries = batteries;

            PostBuildValidation();
        }

		private void PostBuildValidation()
		{
            AssertBuildable(_botIds != null, "botIds are mandatory");
            AssertBuildable(_botEnergyMap != null, "botEnergyMap is mandatory");
            AssertBuildable(_width > 0, "planWidth must be positive");
			AssertBuildable(_height > 0, "planHeight must be positive");
			AssertBuildable(HasUniqueValues(_botIds), "duplicate values in botIds");
			AssertBuildable(HasUniqueValues(_botLocationMap.Values), "duplicate bot locations");
			AssertLocationHashSetForAllLiveBots();
			AssertMultipleObjectsDoNotOccupyTheSameLocation();
		}

		private void AssertLocationHashSetForAllLiveBots()
		{
			foreach (var botId in _liveBotIds)
				if (_botLocationMap[botId] == null)
					throw new Exception("location not set for the bot " + botId);
		}


		private void AssertMultipleObjectsDoNotOccupyTheSameLocation()
		{
		    var allLocations = new List<Point>();
		    allLocations.AddRange(_botLocationMap.Values);
		    allLocations.AddRange(_obstacles);
		    allLocations.AddRange(_treasures);
		    allLocations.AddRange(_batteries);
		    AssertBuildable(HasUniqueValues(allLocations), "multiple objects may not occupy the same location");
		}

		private static void AssertBuildable(bool assertion, string message)
		{
			if (!assertion)
				throw new Exception(message);
		}

		private static bool HasUniqueValues<T>(ICollection<T> collection)
		{
			return collection.Count == new HashSet<T>(collection).Count;
		}

		private void AssertKnownBotId(int botId)
		{
			if (!_botIds.Contains(botId))
				throw new Exception("unknown bot ID: " + botId);
		}
	}
}