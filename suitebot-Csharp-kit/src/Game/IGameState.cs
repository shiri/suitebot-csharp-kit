﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace suitebot_Csharp_kit.Game
{
	public interface IGameState
	{
		/**
		 * Returns the width of the game plan.
		 *
		 * @return the width of the game plan
		 */
		int GetPlanWidth();

		/**
		 * Returns the height of the game plan.
		 *
		 * @return the height  of the game plan
		 */
		int GetPlanHeight();

		/**
		 * Returns the list of the IDs of all bots, including the dead ones.
		 *
		 * @return the list of the IDs of all bots
		 */
		List<int> GetAllBotIds();

		/**
		 * Returns the set of the IDS of all live bots, i.e. the bots that are still active in the game.
		 *
		 * @return the set of the IDS of all live bots
		 */
		List<int> GetLiveBotIds();

		/**
		 * Returns the coordinates of the location of the bot on the game plan.
		 *
		 * @param botId ID of the bot
		 * @return the location of the bot or null if the bot is dead
		 * @throws IllegalArgumentException if the bot ID is unknown
		 */
		Point GetBotLocation(int botId);

		/**
		 * Returns current energy of the bot. The energy is a non-negative number.
		 *
		 * @param botId ID of the bot
		 * @return current energy of the bot
		 * @throws IllegalArgumentException if the bot ID is unknown
		 */
		int GetBotEnergy(int botId);

		/**
		 * Returns the set of coordinates of all obstacles on the game plan.
		 *
		 * @return the set of coordinates of all obstacles
		 */
		HashSet<Point> GetObstacleLocations();

        /**
		 * Returns the set of coordinates of all treasures on the game plan.
		 *
		 * @return the set of coordinates of all treasures
		 */
        HashSet<Point> GetTreasureLocations();

        /**
		 * Returns the set of coordinates of all batteries on the game plan.
		 *
		 * @return the set of coordinates of all batteries
		 */
        HashSet<Point> GetBatteryLocations();
	}
}