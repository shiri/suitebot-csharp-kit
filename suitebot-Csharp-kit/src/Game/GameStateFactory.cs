﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace suitebot_Csharp_kit.Game
{
	public class GameStateFactory
	{
		private const string ExceptionMessage =
            "non-rectangular plan: line {0} width ({1}) is different from the line 1 width {2}";
		public const char Obstacle = '*';
		public const char Treasure = '!';
		public const char Battery = '+';
		public const char Empty = ' ';

		public const int DefaultBotEnergy = 10;

	    public static IGameState CreateFromContract(Json.GameStateContract gameStateContract)
	    {
	        return CreateFromContract(gameStateContract, DefaultBotEnergy);
	    }

        public static IGameState CreateFromContract(Json.GameStateContract gameStateContract, int botEnergy)
        {
            var botIds = new List<int>();
            
            var botLocationMap = new Dictionary<int, Point>();
            var botEnergyMap = gameStateContract.BotEnergyMap.ToDictionary(k => int.Parse(k.Key), k => k.Value);
            var obstacles = new HashSet<Point>();
            var treasures = new HashSet<Point>();
            var batteries = new HashSet<Point>();

            var lines = gameStateContract.GamePlan;
            AssertRectangularPlan(lines);

            var width = lines[0].Length;
            var height = lines.Count;

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    var ch = lines[y][x];
                    var location = new Point(x, y);

                    switch (ch)
                    {
                        case Obstacle:
                            obstacles.Add(location);
                            break;
                        case Treasure:
                            treasures.Add(location);
                            break;
                        case Battery:
                            batteries.Add(location);
                            break;
                        default:
                            if (char.IsDigit(ch))
                            {
                                var botId = int.Parse(ch.ToString());
                                botIds.Add(botId);
                                botLocationMap[botId] = location;
                                botEnergyMap[botId] = botEnergy;
                            }
                            else if (ch != Empty)
                                throw new Exception("unrecognized character: " + ch);
                            break;
                    }
                }
            }

            return new ImmutableGameState(
                    width,
                    height,
                    botIds,
                    botEnergyMap,
                    botLocationMap,
                    obstacles,
                    treasures,
                    batteries
                );
        }

        private static void AssertRectangularPlan(IReadOnlyList<string> lines)
		{
			var width = lines[0].Length;

			for (var i = 1; i < lines.Count; i++)
			{
				if (lines[i].Length == width)
					continue;

				var errMsg = string.Format(ExceptionMessage , i + 1, lines[i].Length, width);
				throw new Exception(errMsg);
			}
		}
	}
}

