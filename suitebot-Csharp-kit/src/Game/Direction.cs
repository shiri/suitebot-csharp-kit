﻿namespace suitebot_Csharp_kit.Game
{
	public class Direction
	{
		public static readonly Direction Up = new Direction(0, -1, "U");
		public static readonly Direction Down = new Direction(0, 1, "D");
		public static readonly Direction Left = new Direction(-1, 0, "L");
        public static readonly Direction Right = new Direction(1, 0, "R");

		internal readonly int XChange;
		internal readonly int YChange;
		private readonly string _stringRepresentation;

		private Direction(int xChange, int yChange, string stringRepresentation)
		{
			XChange = xChange;
			YChange = yChange;
			_stringRepresentation = stringRepresentation;
		}

        public Point FromPosition(Point position)
        {
            return new Point(position.X + XChange, position.Y + YChange);
        }

        public static Point FromPosition(Point position, Direction direction)
		{
			return new Point(position.X + direction.XChange, position.Y + direction.YChange);
		}

		public override string ToString()
		{
			return _stringRepresentation;
		}
	}
}