﻿namespace suitebot_Csharp_kit.Game
{
	public class Point
	{
		public readonly int X;
		public readonly int Y;

		public Point(int x, int y)
		{
			Y = y;
			X = x;
		}


		public override bool Equals(object o)
		{
			var point = o as Point;
			if (point == null)
				return false;

			return X == point.X && Y == point.Y;
		}

		public override int GetHashCode()
		{
			var result = X;
			result = 31*result + Y;
			return result;
		}

		public override string ToString()
		{
			return "Point{" +
			       "x=" + X +
			       ", y=" + Y +
			       '}';
		}
	}
}