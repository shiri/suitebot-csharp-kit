﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace suitebot_Csharp_kit.Json
{
	[DataContract]
	public class GameStateContract
	{
		[DataMember(Name = "yourBotId")]
		public int YourBotId;

		[DataMember(Name = "gamePlan")]
		public List<string> GamePlan;

		[DataMember(Name = "botIds")]
		public List<int> BotIds;

		[DataMember(Name = "botEnergyMap")]
		public IDictionary<string, int> BotEnergyMap;

        public static DataContractJsonSerializer GetSerializer()
        {
            var settings = new DataContractJsonSerializerSettings {UseSimpleDictionaryFormat = true};

            return new DataContractJsonSerializer(typeof(GameStateContract), settings);
            
        }
    }
}