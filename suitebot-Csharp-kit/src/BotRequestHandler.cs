﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using suitebot_Csharp_kit.BotAI;
using suitebot_Csharp_kit.Json;
using suitebot_Csharp_kit.Server;

namespace suitebot_Csharp_kit
{
	public class BotRequestHandler : ISimpleRequestHandler
	{
		public const string NameRequest = "NAME";
		private readonly IBotAi _botAi;

		public BotRequestHandler(IBotAi botAi)
		{
			_botAi = botAi;
        }

		public string ProcessRequest(string request)
		{
			try
			{
				return ProcessRequestInternal(request);
			}
			catch (Exception e)
			{
				return e.ToString();
			}
		}

		private string ProcessRequestInternal(string request)
		{
			return NameRequest == request ? _botAi.GetName() : ProcessMoveRequest(request);
		}

		private string ProcessMoveRequest(string request)
		{
			var gameStateContract = Newtonsoft.Json.JsonConvert.DeserializeObject<GameStateContract>(request);
            var gameState = Game.GameStateFactory.CreateFromContract(gameStateContract);
			var move = _botAi.MakeMove(gameState, gameStateContract.YourBotId);
			return move != null ? move.ToString() : "";
		}
	}
}
