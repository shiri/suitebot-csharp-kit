﻿using suitebot_Csharp_kit.Game;

namespace suitebot_Csharp_kit.BotAI
{
	public interface IBotAi
	{
		/**
		 * Returns the move that the AI intends to play.
		 *
		 * @param botId ID of the bot operated by the AI
		 * @param gameState current game state
		 * @return the move that the AI intends to play
		 */
		Move MakeMove(IGameState gameState, int botId);

		/**
		 * Returns the name of the bot.
		 *
		 * @return the name of the bot
		 */
		string GetName();
	}
}
