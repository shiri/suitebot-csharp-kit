﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using suitebot_Csharp_kit.BotAI;
using suitebot_Csharp_kit.server;
using System;
using System.Text;
using suitebot_Csharp_kit.Game;
using System.Runtime.Serialization.Json;
using System.IO;

namespace suitebot_Csharp_kit.Tests
{
    [TestClass()]
    public class BotRequestHandlerTests
    {
        private const string GameStateJSON =
			"{\n" +
			"  \"yourBotId\": 2,\n" +
			"  \"botIds\": [1, 2, 4],\n" +
			"  \"botEnergyMap\": {\n" +
			"    \"1\": 0,\n" +
			"    \"2\": 15,\n" +
			"    \"4\": 18\n" +
			"  },\n" +
			"  \"gamePlan\": [\n" +
			"    \"*2 *\",\n" +
			"    \"4 +*\",\n" +
			"    \"!   \"\n" +
			"  ]\n" +
			"}";

        [TestMethod()]
        public void ProcessRequestNameTest()
        {
            IBotAi botAi = new TestBotAi();
            ISimpleRequestHandler requestHandler = new BotRequestHandler(botAi);
            Assert.AreEqual("Test", requestHandler.ProcessRequest("NAME"));
        }

        [TestMethod()]
        public void DeserializeGameStateTest()
        {
            DataContractJsonSerializer serializer = Json.GameStateContract.GetSerializer();
            var obj = (Json.GameStateContract) serializer.ReadObject(new MemoryStream(Encoding.ASCII.GetBytes(GameStateJSON)));

            Assert.AreEqual(2, obj.YourBotId);
            Assert.AreEqual(3, obj.BotIds.Count);
            Assert.AreEqual(3, obj.BotEnergyMap.Count);
            Assert.AreEqual(3, obj.GamePlan.Count);
        }

        [TestMethod()]
        public void ProcessRequestTest()
        {
            IBotAi botAi = new TestBotAi();
            ISimpleRequestHandler requestHandler = new BotRequestHandler(botAi);
            Assert.AreNotEqual("Test", requestHandler.ProcessRequest(GameStateJSON));
        }


        private class TestBotAi : IBotAi
        {
            public string GetName()
            {
                return "Test";
            }

            public Move MakeMove(IGameState gameState, int botId)
            {
                throw new NotImplementedException();
            }
        }
    }
}